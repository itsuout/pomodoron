Feature: Log

    Rule: a todo should be logged after working session is done
    Scenario: a user have assigned a task and working on a todo
        Given working session have started
        When timer is ended
        Then a todo should be logged under current task


    Rule: a log should be stacked for a continuos todo
    Scenario: a user have assigned a task and working on a todo
        Given working session have started
        When timer is ended
        And a todo have logged once
        Then a logged to do should have be updated
