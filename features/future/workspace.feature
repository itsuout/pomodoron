Feature: Workspace

    # Implemented
    Rule: Only generate identity after first workspace is going to be created
    Scenario: a user visited /
        Given no workspace is created
        When the user do nothing and leave
        Then gun shouldn't initiated

    # Implemented
    Rule: Recoginize user after gun instace is created
    Scenario: a user visited /
        Given a user had created new workspace
        When a user signout and leave
        Then gun should recognized the user
    # Implemented
    Rule: Every workspaces must have a valid name
    Scenario: a user is creating new workspace
        Given workspace name is given by a user
        And workspace name is a valid name
        When a user press create new workspace button
        Then workspace should have successfully created with default password

    Scenario: User is creating new workspace
        Given workspace name is given by a user
        And workspace name is not a valid name
        When a user press create new workspace button
        Then error message should have displayed to a user

    Scenario: User is creating new workspace
        Given workspace name is given by a user
        And workspace name is duplicated
        When a user press create new workspace button
        Then error message should have displayed to a user

    Rule: User can supply a password for workspace
    Scenario: User is creating workspace
        Given valid workspace name is given by a user
        And workspace password is given by a user
        And workspace password is a valid password 
        When a user press create button
        Then workspace should have successfully created with given password

    Scenario: User is creating workspace
        Given valid workspace name is given by a user
        And workspace password is given by a user
        And workspace password is not a valid password 
        When a user press create button
        Then error message should have displayed to a user

    Rule: Workspace must be created as given inputs
    Scenario: User is creating new workspace
        Given valid workspace and password are given
        When a user press create new workspace button
        Then new workspace should have successfully created as given inputs

    Rule: User can sign in to  workspace
    Scenario: User is signing in to workspace
        Given valid workspace and password of created workspaces are given
        When a user press sign in button
        Then a user should be redirected to /app
        And /app should display a correct workspace

    Scenario: User is signing in to workspace
        Given invalid workspace or password are given 
        When a user press sign in button
        Then a user should be redirected to /app
        And /app should display a correct workspace


    Rule: User can sign in to workspace without typing its name
    Scenario: User is signing in to workspace
        Given There is at leaste one workspace created
        When a user visit /
        And a workspace button that will sign user into workspace appear
        Then a user can sign in to workspace regarding the botton clicked
        And a user need to supply password if the workspace is locked by password

    Rule: User can logout from workspace
    Scenario: User logging out from workspace
        Given a user already signed in to workspace
        When a user press logout button
        Then user successfully log out
        And redirected to /

    Rule: User can delete workspace
    Scenario: User is deleting workspace
        Given a user already logged in to workspace
        When a user press delete workspace button
        And a user press confirm button
        Then curret workspace should be deleted
        And a user should be redirected to /

    Rule: Application must redirect user to a correct page
    Scenario: User access to application page before signed in
        Given a user have not signed in
        When a user visit /app
        Then a user should be redirected to /

    Scenario: User access to home page after signed in
        Given a user already signed in
        When a user visit /
        Then a user should be redirected to /app

