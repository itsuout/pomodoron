Feature: Log

    Rule: done tasks should be logged 
    Scenario: Logging done tasks
        Given timer is ended
        When task is marked as done
        Then task should be logged

    Rule: Log should display done tasks
    Scenario: Log is displaying done tasks
        Given there is no task done
        When log is displaying done tasks
        Then log should display no task done message

    Scenario: Log is displaying done tasks
        Given there is at least one task donae
        When log is displaying done tasks
        Then log should be display done tasks

    Rule: Logged task can be deleted
    Scenario: User using app
        Given done tasks are logged
        When a user clicked a clear log botton
        Then all logged tasks should be cleared from log
