Feature: Session

    # implemented
    Rule: User can create task
    Scenario: User creating a task
        Given timer is not running
        And task name is given
        When timer is started
        Then a task should be created

    # implemented
    Rule: User can edit task name
    Scenario: timer is running
        Given task name already assigned
        When user rename current task
        Then a task should be renamed

    # Implemented
    Rule: Task name should be valid
    Scenario: User assigning task name
        Given task name is empty
        When user start a timer
        Then error message should be displayed

    Scenario: User assigning task name
        Given task name is longer than 75 characters
        When user start a timer
        Then error message should be displayed
        
    # Implemented
    Rule: User can add note to current task
    Scenario: User is using app
        Given timer is running
        When a user type to current task note
        Then current task note should be updated

    Scenario: User is using app
        Given timer is not running
        When a user type to current task note
        Then created note should be next task note

    # Implemented
    Rule: Empty note will not be saved to task
    Scenario: Timer is ended
        Given task note is empty
        When task is marked as done
        Then logged task will not have a note

    # Implemented
    Rule: User inputs must be purified
    Scenario: User is assigning task name
        Given timer is ended
        When task is marked as done
        Then task name should be purified
        And task note should be purified

    # implemented
    Rule: Session must be changed to next session when ended
    Scenario: timer is ended
        Given session is working
        And tomatoes value % 4 != 0
        When timer post message back to the app
        Then session is set to shortbreak

    Scenario: timer is ended
        Given session is working
        And tomatoes value % 4 == 0
        When timer post message back to the app
        Then session is set to longbreak
    
    Scenario: timer is ended
        Given session is either shortbreak or longbreak
        When timer post message back to the app
        Then session is set to working

    # implemented 
    Rule: Task tomatoes must be increase by 1 after working session ended
    Scenario: timer is ended
        Given session is working
        When timeer post message bak to to the app
        Then task tomatoes is increase by one

    # implemented
    Rule: Task can be marked as done only after working session ended
    Scenario: timer is ended
        Given session is working
        When timer post message back to to the app
        Then done button should be displayed
    
    # implemented
    Rule: Task can't be marked as done if new session is started
    Scenario: timer is ended
        Given done button is displayed to user
        When new session is started
        Then done button should be hidden



    # implemented
    Rule: When timer is running inspirational text should be display instead of done button
    Scenario: User is using app
        Given done button is hidden
        When timer is not running
        Then inspirational text should be displayed

    Scenario: User is using app
        Given done button is hidden
        When timer is running
        Then inspirational text should be displayed
