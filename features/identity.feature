Feature: Session

    Rule: Display a introduction prompt for an anonymous user
    Scenario: a user is using the app for the first time
        Given gun instance is not initiated
        When a user visit to /app
        Then an introduction dialogbox should be display

    Rule: Only create gun instace according to a User permission
    Scenario: a user read introduction text and then just leave
        Given gun instant is not initiated
        When a user visit to /app
        And introduction dialogbox is displayed
        And a user do nothing and leave
        Then a gun instance should not be created
