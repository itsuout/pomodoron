Feature: Task

    Rule: task should be assigned before pomodoro session can start
    Scenario: a user start using the app
        Given a user havn't created any task
        When a user is using app
        Then pomodoro form should be disabled

    
    Rule: users should working with one task at a time
    Scenario: a user is trying create new task
        Given a task is already assigned
        When a user is using app
        Then task from should be disabled

    Rule: users should be able to delete created task
    Scenario: a user is trying to create new task
        Given a task is already assigned
        When task option button is clicked
        Then a user should be able to delete current task

    Rule: users should be able to finish task
    Scenario: a user is done with the task
        Given a task is already assigned
        When task option button is clicked
        Then a user should be able to finish task
