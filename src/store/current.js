import { writable, derived } from "svelte/store"
import djs from "../script/init_dayjs.js";

const minute = 1000 * 60
const second = 1000
const duration = {
  production: {
    working: 25 * minute,
    shortbreak: 5 * minute,
    longbreak: 15 * minute
  },
  develop: {
    working: 5 * second,
    shortbreak: 2 * second,
    longbreak: 3 * second
  }
}
const mode = "production"
export const timing = writable(false)
export const session = writable("working")
export const tomatoes = writable(0)
export const accumulativeTomatoes = writable(0)
export const estimatedGlobal = writable(1)

export const timeout = derived(session, ($session, set) => {
  var d;
  switch ($session) {
    case "working":
      d = mode === "production" ? duration.production.working : duration.develop.working
      set(d)
      break
    case "shortbreak":
      d = mode === "production" ? duration.production.shortbreak : duration.develop.shortbreak
      set(d)
      break
    case "longbreak":
      d = mode === "production" ? duration.production.longbreak : duration.develop.longbreak
      set(d)
      break
    default:
      set(duration.develop.working)
      break
  }
}
);

export const timerLabel = derived(timeout, $timeout => djs.utc($timeout).local().format("mmss"))
export const next = derived([session, tomatoes], ([$session, $tomatoes], set) => {
  switch ($session) {
    case "working":
      let n = $tomatoes % 4 != 0 ? "shortbreak" : "longbreak"
      set(n)
      break
    default:
      set("working")
      break
  }
})
