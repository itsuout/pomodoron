import { readable } from 'svelte/store';
import dayjs from '../script/init_dayjs.js'

const second = 1000
const minute = 60 * second
const hour = 60 * minute
const day = 24 * hour

export const month = readable(dayjs.utc().local().format('MMMM'))
export const year = readable(dayjs.utc().local().format('YY'))

export const dow = readable(dayjs.utc().local().format('dddd'), function start(set) {
	const interval = setInterval(() => {
		set(dayjs.utc().local().format('dddd'))
	}, day);

	return function stop() {
		clearInterval(interval);
	};
})

export const dom = readable(dayjs.utc().local().format('DD'), function start(set) {
	const interval = setInterval(() => {
		set(dayjs.utc().local().format('DD'))
	}, day);

	return function stop() {
		clearInterval(interval);
	};
})

export const hours = readable(dayjs.utc().local().format('HH'), function start(set) {
	const interval = setInterval(() => {
		set(dayjs.utc().local().format('HH'))
	}, second);

	return function stop() {
		clearInterval(interval);
	};
});


export const minutes = readable(dayjs.utc().local().format('mm'), function start(set) {
	const interval = setInterval(() => {
		set(dayjs.utc().local().format('mm'))
	}, second);

	return function stop() {
		clearInterval(interval);
	};
});

export const unix = readable(dayjs.utc().local().valueOf(), function start(set) {
	const interval = setInterval(() => {
		set(dayjs.utc().local().valueOf())
	}, 1);

	return function stop() {
		clearInterval(interval);
	};
})

