import { writable } from 'svelte/store'

const buttonEvent = writable("define")

const task = writable({
  name: "",
  opened: 0,
  // utc().local().valueOf() when task is closed
})

const taskID = writable(0)

const todo = writable({
  name: "",
  opened: 0,
  began: 0,
  estimated: 0,
  tomatoes: 0,
  ended: 0
})

const todoID = writable(0)
const isTodoDefined = writable(false)

export { buttonEvent, task, taskID, todo, todoID, isTodoDefined}

