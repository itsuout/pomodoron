import djs from "./init_dayjs.js";
onmessage = (e) => {
  const triggered = djs.utc().valueOf()
  const end = djs.utc(triggered + e.data.timeout)

  let minuteDiff
  let secondDiff
  var current
  const interval = setInterval(() => {
    current = djs.utc(djs.utc().valueOf())
    minuteDiff = end.diff(current, "m");
    secondDiff = end.diff(current, "s") % 60;
    postMessage(
      `${minuteDiff.toString().padStart(2, "0")}${secondDiff
        .toString()
        .padStart(2, "0")}`
    );
  }, 1000)
  setTimeout(() => {
    clearInterval(interval)
  }, e.data.timeout)
}
