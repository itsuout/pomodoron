import DOMPurify from "dompurify";


const MinMaxChars = async (type, content, min, max, errMessage = { over: "", under: "", empty: "" }) => {
  content = DOMPurify.sanitize(content);
  var err;
  if (content.length > max) {
    err = errMessage.over !== "" ? errMessage.over : `${type} must be a string shorter than ${max} charracters`
    return Promise.reject(err)
  }

  if (content.length == 0) {
    err = errMessage.empty !== "" ? errMessage.empty : `${type} can't be empty`
    return Promise.reject(err)
  }

  if (content.length < min) {
    err = errMessage.under !== "" ? errMessage.under : `${type} must contain at least ${min} charracters`
    return Promise.reject(err)
  }


  return Promise.resolve(content)
}

const MaxChars = async (type, content, max, errMessage = "") => {
  content = DOMPurify.sanitize(content);
  if (content.length > max) {
    const err = errMessage !== "" ? errMessage : `${type} must be a string shorter than ${max} charracters`
    return Promise.reject(err)

  }
  return Promise.resolve(content)
}

const MinMaxInt = (type, content, min, max, errMessage = { over: "", under: "", empty: "" }) => {
  var err;
  if (typeof(content) != typeof(0)) {
    return Promise.reject("estimated tomatoes must be number")
  }
  if (!isFinite(content)) {
    err = "tteka, it's like infinity here"
    return Promise.reject(err)
  }

  if (content == null) {
    err = errMessage.empty !== "" ? errMessage.empty : `${type} can't be empty`
    return Promise.reject(err)
  }
  if (content > max) {
    err = errMessage.over !== "" ? errMessage.over : `${type} must be an integer smaller than ${max}`
    return Promise.reject(err)
  }

  if (content < min) {
    err = errMessage.under !== "" ? errMessage.under : `${type} must be an integer larger or equal to ${min}`
    return Promise.reject(err)
  }
  return Promise.resolve(content)
}

export { MinMaxChars, MaxChars, MinMaxInt }
