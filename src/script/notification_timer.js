import djs from "./init_dayjs.js"
import Counter from "./notification_counter.js?worker"
//
// Feature: Notification
// Rule: only send notificatiol to after permission is granted
const notifyMe = (s) => {
  if (Notification.permission === "granted") {
    let img = "/tomato.png"
    var notification = new Notification('Pomodoron', { body: `your ${s} session has done`, icon: img });
  }
};

onmessage = function(e) {
  const begin = djs.utc().local().valueOf()
  setTimeout(() => {
    notifyMe(e.data.session)
    postMessage({begin: begin})
  }, e.data.timeout + 1)
    
}

