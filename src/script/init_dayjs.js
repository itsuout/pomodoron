import dayjs from 'dayjs' 
import utc from 'dayjs/plugin/utc'

const djs = dayjs.extend(utc)
export default djs
