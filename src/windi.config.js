import lineClamp from "windicss/plugin/line-clamp"
export default {
  extend: {
    lineClamp: {
      sm: '3',
      lg: '10',
    },
  },
  plugins: [
    lineClamp
  ],
}
