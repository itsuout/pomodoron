import { fileURLToPath } from 'url';
import { dirname, resolve } from 'path';
import { svelte } from '@sveltejs/vite-plugin-svelte'
import WindiCSS from 'vite-plugin-windicss'

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const moduleExclude = match => {
  const m = id => id.indexOf(match) > -1
  return {
    name: `exclude-${match}`,
    resolveId(id) {
      if (m(id)) return id
    },
    load(id) {
      if (m(id)) return `export default {}`
    },
  }
}

export default {
  optimizeDeps: {
    include: [
      'gun',
      'gun/gun',
      'gun/sea',
      'gun/sea.js',
      'gun/lib/then',
      'gun/lib/webrtc',
      'gun/lib/radix',
      'gun/lib/radisk',
      'gun/lib/store',
      'gun/lib/rindexed',
    ],
  },
  plugins: [
    moduleExclude('text-encoding'),
    svelte({
    }),
    WindiCSS()
  ],
  build: {
    rollupOptions: {
      input: {
       index: resolve(__dirname, 'index.html'),
      }
    }
  }
}
