# 🍅 [Pomodoron](https://pomodoron.netlify.app): a task-based pomodoro timer
---

[![Netlify Status](https://api.netlify.com/api/v1/badges/ab9033c0-32ef-40fd-bcf5-b4a5389e4701/deploy-status)](https://app.netlify.com/sites/pomodoron/deploys)



## Why
### 📝 To getting better at capturing the present
  Pomodoron is designed to be a tool that should be used on the most atomic part of todo. It is not designed to be a one-stop to-do tool.

### 🔧 Usage
To get the best out of Pomodoron is to use it as a complementary of other todo tools. (e.g., Trello and others)

## Notice

### 💾 Saving session using Gun
After the init database is clicked, Gun instance will be saved to your local storage. It can be deleted and re-initiated anytime.

### 🔔 Work best with notification
Pomodoron will be able to send notification only if notification permission is granted.

### ⌨️ Fully support keyboard navigation
Pomodoron can be navigated through a tab and shift+tab.

## To-dos
- Multiple Workspaces
- Multiple Tasks
- Multiple To-dos



###### currently not supported on mobile


---

[MIT License](LICENSCE.md)

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://buymeacoffee.com/dashft)
