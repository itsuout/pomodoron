import { windi } from "svelte-windicss-preprocess";
const config = {
  preprocess: [
    windi({})
  ],
};

export default config;
